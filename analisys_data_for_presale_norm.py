import pyexcel as p
from openpyxl import Workbook, load_workbook
import hashlib
import re
import os
from tqdm import tqdm
from time import sleep
import requests
import json
from datetime import datetime
from collections import Counter


class PreSaleAnalisys:
    """Класс, формирующий файл с анализом данных по ошибкам в них.
    На вход необходимо передать адрес (path_file) и имя(file_name) анализируемого файла
    Формат файла, должен бытьЖ
    |  id  |        namefull        |        charnname        |
    |  01  | Название материала №1 | Характеристика м-ла №1 |
    |  02  | Название материала №2 | Характеристика м-ла №2 |
    |  03  | Название материала №3 | Характеристика м-ла №3 |"""

    def __init__(self, path_file, file_name):
        self.path_file = path_file
        self.file_name = file_name
        self.name_sheet = load_workbook(os.path.join(self.path_file, self.file_name)).sheetnames[0]
        self.data = p.get_book_dict(file_name=os.path.join(self.path_file, self.file_name))
        print('Формирование словаря данных')
        self.data_dict = {str(data[0]): (str(data[1]) + ' ' + str(data[2])) for data in
                          tqdm(self.data[self.name_sheet][1:])}

    def get_hash(self, string_for_hash):
        """Метод для хэширования строк"""
        lip_row = string_for_hash.lower().replace(' ', '').encode()
        hash_row = hashlib.md5(lip_row).hexdigest()
        return hash_row

    def get_stat(self, data_dict, count_all_rows, time_analisys):
        """Метод получения статистики из аналитики"""
        result = {}
        id_group = 1
        for val, ids in data_dict.items():
            if len(ids) > 1:
                result['group_{}'.format(id_group)] = ids
                id_group += 1
        count_doubles_rows = sum((len(i) for i in data_dict.values() if len(i) > 1))
        prc = 100 * count_doubles_rows / count_all_rows

        return (count_doubles_rows, prc, result, time_analisys)

    def split(self, arr, size):
        """Метод разбивки массива на массивы длины size"""
        arrs = []
        while len(arr) > size:
            pice = arr[:size]
            arrs.append(pice)
            arr = arr[size:]
        arrs.append(arr)
        return arrs

    def get_vals_by_id(self, id):
        """Метод получения значения по ID строки"""
        return self.data_dict[id]

    def analisys_double_row_fullname(self):
        """Метод поиска дублей по полному совпадению наименования
        Пример вывода результата:
        (10401, 7.8831286948613, {'group_1': [26, 58439, 94249], 'group_2': [30, 68407],...}"""
        start_time = datetime.now()

        data_dict = {}
        count_all_rows = 0
        print('Поиск дублей по полному совпадению наименования')
        sleep(0.5)
        for id, namefull in tqdm(self.data_dict.items()):
            hash_row = self.get_hash(namefull.lower())
            try:
                data_dict[hash_row].append(id)
                sorted(data_dict[hash_row])
            except KeyError:
                data_dict[hash_row] = [id]
            count_all_rows += 1
        time_analisys = (datetime.now() - start_time).total_seconds()

        return self.get_stat(data_dict, count_all_rows, time_analisys)

    def analisys_double_row_potential(self):
        """Метод поиска потенциальных дублей
        Пример вывода результата:
        (10401, 7.8831286948613, {'group_1': [26, 58439, 94249], 'group_2': [30, 68407],...}"""
        start_time = datetime.now()

        data_dict = {}
        count_all_rows = 0
        print('Поиск потенциальных дублей')
        sleep(0.5)
        for id, namefull in tqdm(self.data_dict.items()):
            arr_namefull = set(sorted(re.split('[ .,;:!@#$%"<>^&*()_+''|\\/?=\xa0\s\t]', namefull.lower())))
            hash_row = self.get_hash(''.join(arr_namefull))
            try:
                data_dict[hash_row].append(id)
                sorted(data_dict[hash_row])
            except KeyError:
                data_dict[hash_row] = [id]
            count_all_rows += 1
        time_analisys = (datetime.now() - start_time).total_seconds()

        return self.get_stat(data_dict, count_all_rows, time_analisys)

    def analisys_double_row_similar_name(self):
        """Метод поиска дублей с наименованием, где по разному располагаются слова
        Пример вывода результата:
        (10401, 7.8831286948613, {'group_1': [26, 58439, 94249], 'group_2': [30, 68407],...}"""
        start_time = datetime.now()

        data_dict = {}
        count_all_rows = 0
        print('Поиск дублей с наименованием, где по разному располагаются слова')
        sleep(0.5)
        for id, namefull in tqdm(self.data_dict.items()):
            arr_namefull = set(sorted(namefull.lower().split()))
            hash_row = self.get_hash(''.join(arr_namefull))
            try:
                data_dict[hash_row].append(id)
                sorted(data_dict[hash_row])
            except KeyError:
                data_dict[hash_row] = [id]
            count_all_rows += 1
        double_row_fullname = self.analisys_double_row_fullname()[2]
        double_row_similar_name = self.get_stat(data_dict, count_all_rows, 0)
        result = {}
        id_group = 1
        print('Исключение полных дублей из дублей с наименованием, где по разному располагаются слова')
        sleep(0.5)
        for group, ids in tqdm(double_row_similar_name[2].items()):
            for g, i in double_row_fullname.items():
                over = set(ids) - set(i)
                if set(ids) != over and len(over) != 0:
                    result['group_{}'.format(id_group)] = [i[0]] + [i for i in over]
                    id_group += 1

        time_analisys = (datetime.now() - start_time).total_seconds()

        return self.get_stat(result, count_all_rows, time_analisys)

    def analisys_first_classifier(self):
        """Метод первичной классификации
        Пример вывода результата:
        (10401, 7.8831286948613, {'group_1': [26, 58439, 94249], 'group_2': [30, 68407],...}"""
        start_time = datetime.now()

        data_dict = {}
        count_all_rows = 0
        print('Первичная классифкация')
        sleep(0.5)
        for id, namefull in tqdm(self.data_dict.items()):
            arr_namefull_only_words = set(sorted(re.findall('[a-яA-Z]{4,}', namefull.lower())))
            hash_row = self.get_hash(''.join(arr_namefull_only_words))
            try:
                data_dict[hash_row].append(id)
            except KeyError:
                data_dict[hash_row] = [id]
            count_all_rows += 1

        time_analisys = (datetime.now() - start_time).total_seconds()

        return self.get_stat(data_dict, count_all_rows, time_analisys)

    def analisys_row_begin_int(self):
        """Метод поиска строк, которые начинаются с числа
        Пример вывода результата:
        (168, 0.1273306048203729, [13, 40, 3365, 3852, 3874, 5322, 6948,...]}"""
        start_time = datetime.now()

        data_dict = []
        count_all_rows = 0
        print('Поиск строк, которые начинаются с числа')
        sleep(0.5)
        for id, namefull in tqdm(self.data_dict.items()):
            count_all_rows += 1
            try:
                try:
                    int(namefull[0])
                except ValueError:
                    continue
                else:
                    data_dict.append(id)
            except IndexError:
                continue
        prc = 100 * len(data_dict) / count_all_rows

        time_analisys = (datetime.now() - start_time).total_seconds()

        return (len(data_dict), prc, data_dict, time_analisys)

    def analisys_row_begin_latin(self):
        """Метод поиска строк, которые начинаются с латинской буквы
        Пример вывода результата:
        (168, 0.1273306048203729, [13, 40, 3365, 3852, 3874, 5322, 6948,...]}"""
        start_time = datetime.now()

        data_dict = []
        count_all_rows = 0
        print('Поиск строк, которые начинаются с латинской буквы')
        sleep(0.5)
        for id, namefull in tqdm(self.data_dict.items()):
            count_all_rows += 1
            try:
                letter = re.findall('[a-zA-Z]', namefull[0])
                if len(letter) == 1:
                    data_dict.append(id)
            except IndexError:
                continue
        prc = 100 * len(data_dict) / count_all_rows

        time_analisys = (datetime.now() - start_time).total_seconds()

        return (len(data_dict), prc, data_dict, time_analisys)

    def analisys_row_begin_adj(self):
        """Метод поиска строк, которые начинаются с прилагательных
        Пример вывода результата:
        (168, 0.1273306048203729, [13, 40, 3365, 3852, 3874, 5322, 6948,...]}"""
        start_time = datetime.now()

        end_of_adj = ('ой', 'ей', 'ому', 'ему', 'ой', 'ый', 'ий', 'ья', 'ые', 'ье', 'ого', 'ая', 'ое', 'яя', 'ие', 'ее')
        data_dict = []
        count_all_rows = 0
        print('Поиск строк, которые начинаются с прилагательных')
        sleep(0.5)
        for id, namefull in tqdm(self.data_dict.items()):
            count_all_rows += 1
            try:
                end_word = namefull.split()[0][-2:].lower()
                if end_word in end_of_adj:
                    data_dict.append(id)
            except IndexError:
                continue
        prc = 100 * len(data_dict) / count_all_rows

        time_analisys = (datetime.now() - start_time).total_seconds()

        return (len(data_dict), prc, data_dict, time_analisys)

    def analisys_row_begin_symbol(self):
        """Метод поиска строк, которые начинаются со спецсимволов [ .,;:!@#$%"<>^&*()_+''|\\/?=]
        Пример вывода результата:
        (168, 0.1273306048203729, [13, 40, 3365, 3852, 3874, 5322, 6948,...]}"""
        start_time = datetime.now()

        data_dict = []
        count_all_rows = 0
        print('Поиск строк, которые начинаются со спецсимволов [ .,;:!@#$%"<>^&*()_+''|\\/?=]')
        sleep(0.5)
        for id, namefull in tqdm(self.data_dict.items()):
            count_all_rows += 1
            try:
                letter = re.findall('[ .,;:!@#$%"<>^&*()_+''|\\/?=\xa0\s\t]', namefull[0])
                if len(letter) == 1:
                    data_dict.append(id)
            except IndexError:
                continue
        prc = 100 * len(data_dict) / count_all_rows

        time_analisys = (datetime.now() - start_time).total_seconds()

        return (len(data_dict), prc, data_dict, time_analisys)

    def analisys_row_latin_inside_ruword(self):
        """Метод поиска латинских букв внутри русских слов
        Пример вывода результата:
        (61, 0.04623313627406397, {2541: {'ТHВД': ['H']}, 6677: {'НGМ': ['G']}, 7159: {'Маслопpиемник':"""
        start_time = datetime.now()

        data_dict = {}
        count_all_rows = 0
        print('Поиск латинских букв внутри русских слов')
        sleep(0.5)
        for id, namefull in tqdm(self.data_dict.items()):
            count_all_rows += 1
            name_arr = re.findall('[a-яA-Z]{3,}', namefull)
            for word in name_arr:
                first_last_letter = re.findall('[а-яА-я]', word[:1]) + re.findall('[а-яА-я]', word[-1:])
                if len(first_last_letter) == 2:
                    search_latin = re.findall('[a-zA-Z]', word)
                    if len(search_latin) != 0:
                        data_dict[id] = {}
                        data_dict[id][word] = search_latin
        prc = 100 * len(data_dict.keys()) / count_all_rows

        time_analisys = (datetime.now() - start_time).total_seconds()

        return (len(data_dict.keys()), prc, data_dict, time_analisys)

    def analisys_row_cyrillic_inside_enword(self):
        """Метод поиска русских букв внутри латинских слов
        Пример вывода результата:
        (67, 0.05078065787479157, {5971: {'ZAFАC': ['А']}, 10104: {'Cоlor': ['о']}, 10427: {'CНAMPION': ['Н']},"""
        start_time = datetime.now()

        data_dict = {}
        count_all_rows = 0
        print('Поиск русских букв внутри латинских слов')
        sleep(0.5)
        for id, namefull in tqdm(self.data_dict.items()):
            count_all_rows += 1
            name_arr = re.findall('[a-яA-Z]{3,}', namefull)
            for word in name_arr:
                first_last_letter = re.findall('[a-zA-Z]', word[:1]) + re.findall('[a-zA-Z]', word[-1:])
                if len(first_last_letter) == 2:
                    search_cyrillic = re.findall('[а-яА-я]', word)
                    if len(search_cyrillic) != 0:
                        data_dict[id] = {}
                        data_dict[id][word] = search_cyrillic
        prc = 100 * len(data_dict.keys()) / count_all_rows

        time_analisys = (datetime.now() - start_time).total_seconds()

        return (len(data_dict.keys()), prc, data_dict, time_analisys)

    def analisys_row_symbol_inside_word(self):
        """Метод поиска спецсимволов внутри слов
        Пример вывода результата:
        (248, 0.1879642261634076, {1: {'пробле&*^сковый': ['&', '^']}, 359: {'GL&V': ['&']},"""
        start_time = datetime.now()

        data_dict = {}
        count_all_rows = 0
        print('Поиск спецсимволов внутри слов')
        sleep(0.5)
        for id, namefull in tqdm(self.data_dict.items()):
            count_all_rows += 1
            name_arr = re.findall('[a-яA-Z.,;:!@#$%"<>^&*()_+''|\\/?=]{3,}', namefull)
            for word in name_arr:
                first_last_letter = re.findall('[a-яA-Z]', word[:1]) + re.findall('[a-яA-Z]', word[-1:])
                if len(first_last_letter) == 2:
                    search_symbol = re.findall('[!@#$%<>^&_+''|?]', word[1:-1])
                    if len(search_symbol) != 0:
                        data_dict[id] = {}
                        data_dict[id][word] = search_symbol
        prc = 100 * len(data_dict.keys()) / count_all_rows

        time_analisys = (datetime.now() - start_time).total_seconds()

        return (len(data_dict.keys()), prc, data_dict, time_analisys)

    def analisys_row_yo_inside_word(self):
        """Метод поиска буквы Ё внутри слов
        Пример вывода результата:
        (858, 0.6502955889040473, {36: {'жёлтый': ['ё']}, 49: {'жёлтый': ['ё']}, 63:"""
        start_time = datetime.now()

        data_dict = {}
        count_all_rows = 0
        print('Поиск буквы Ё внутри слов')
        sleep(0.5)
        for id, namefull in tqdm(self.data_dict.items()):
            count_all_rows += 1
            name_arr = re.split('[ .,;:\xa0\s]', namefull)
            for word in name_arr:
                search_yo = re.findall('[ёЁ]', word)
                if len(search_yo) != 0:
                    data_dict[id] = {}
                    data_dict[id][word] = search_yo
        prc = 100 * len(data_dict.keys()) / count_all_rows

        time_analisys = (datetime.now() - start_time).total_seconds()

        return (len(data_dict.keys()), prc, data_dict, time_analisys)

    def analisys_row_one_word(self):
        """Метод поиска строк состоящих из одного слова
        Пример вывода результата:
        (168, 0.1273306048203729, [13, 40, 3365, 3852, 3874, 5322, 6948,...]}"""
        start_time = datetime.now()

        data_dict = []
        count_all_rows = 0
        print('Поиск строк состоящих из одного слова')
        sleep(0.5)
        for id, namefull in tqdm(self.data_dict.items()):
            count_all_rows += 1
            arr_name = [i for i in re.split('[ .,;:!@#$%"<>^&*()_+|\/?=\xa0\s]', namefull) if i != '']
            if len(arr_name) == 1:
                data_dict.append(id)
        prc = 100 * len(data_dict) / count_all_rows

        time_analisys = (datetime.now() - start_time).total_seconds()

        return (len(data_dict), prc, data_dict, time_analisys)

    def analisys_row_with_non_breaking_space(self):
        """Метод поиска неразрывного пробела '\xa0'
        Пример вывода результата:
        (858, 0.6502955889040473, {36: {'Пресс-солидол Ж': 2}, 49: {'ГОСТ 1033-79': 1}, 63:"""
        start_time = datetime.now()

        data_dict = {}
        count_all_rows = 0
        print('Поиск неразрывного пробела ''\xa0'' внутри слов')
        sleep(0.5)
        for id, namefull in tqdm(self.data_dict.items()):
            count_all_rows += 1
            if '\xa0' in namefull:
                name_arr = namefull.split(' ')
                for part in name_arr:
                    if '\xa0' in part:
                        data_dict[id] = {}
                        data_dict[id][part] = len(re.split('\xa0', part))
        prc = 100 * len(data_dict.keys()) / count_all_rows

        time_analisys = (datetime.now() - start_time).total_seconds()

        return (len(data_dict.keys()), prc, data_dict, time_analisys)

    def analisys_row_with_tab_space(self):
        """Метод поиска пробела в виде tab '\t'
        Пример вывода результата:
        (858, 0.6502955889040473, {36: {'Пресс-солидол Ж': 2}, 49: {'ГОСТ 1033-79': 1}, 63:"""
        start_time = datetime.now()

        data_dict = {}
        count_all_rows = 0
        print('Поиск пробела в виде tab ''\t'' внутри слов')
        sleep(0.5)
        for id, namefull in tqdm(self.data_dict.items()):
            count_all_rows += 1
            if '\t' in namefull:
                name_arr = namefull.split(' ')
                for part in name_arr:
                    if '\t' in part:
                        data_dict[id] = {}
                        data_dict[id][part] = len(re.split('\t', part))
        prc = 100 * len(data_dict.keys()) / count_all_rows

        time_analisys = (datetime.now() - start_time).total_seconds()

        return (len(data_dict.keys()), prc, data_dict, time_analisys)

    def analisys_row_with_spelling_mistakes(self):
        # TODO добавить проверку на НЕОТВЕТ от сервера яндекса
        """Метод поиска орфографических ошибок
        Пример вывода результата:
        (858, 0.6502955889040473, {36: ['сАлидол'], 49: ['стОндарт'], 63:"""
        start_time = datetime.now()

        data_dict = {}
        count_all_rows = 0
        texts = []
        for id, row in self.data_dict.items():
            texts.append(str(id) + ' ' + str(row))
        packs = self.split(texts, 20)
        print('Поиск орфографических ошибок')
        sleep(0.5)
        for pack in tqdm(packs):
            req = 'https://speller.yandex.net/services/spellservice.json/checkTexts?'
            for txt in pack:
                text = '&text=' + ' '.join(re.findall('[А-я]{5,}', txt))
                req += text
            r = requests.get(req)

            answer = json.dumps(r.json())
            data = json.loads(answer)
            for id, ans in enumerate(data):
                count_all_rows += 1
                if len(ans) > 0:
                    id_mtr = pack[id].split()[0]
                    errors = ''
                    for error in ans:
                        errors += ', ' + error['word'] + ' - ' + ', '.join(error['s'])
                    data_dict[id_mtr] = errors

        prc = 100 * len(data_dict.keys()) / count_all_rows

        time_analisys = (datetime.now() - start_time).total_seconds()

        return (len(data_dict.keys()), prc, data_dict, time_analisys)

    def analisys_row_with_upper_lower_name(self):
        """Подсчет наименований в верхнем регистре и в нижнем регистре
        (858, 0.6502955889040473, {'lower': [1,2,3,], 'upper': [5,6,7]"""
        start_time = datetime.now()

        data_dict = {'lower': [],
                     'upper': [],
                     'fifty': []}
        count_all_rows = 0
        print('Подсчет наименований в верхнем регистре и в нижнем регистре')
        sleep(0.5)
        for id, namefull in tqdm(self.data_dict.items()):
            count_all_rows += 1
            name_arr = re.findall('[a-яA-Z]{4,}', namefull)
            words = []
            for word in name_arr:
                if word.istitle() or word.islower():
                    words.append('lower')
                elif word.isupper():
                    words.append('upper')
            count = Counter(words)
            try:
                test = count['upper']
            except KeyError:
                count['upper'] = 0
            try:
                test = count['lower']
            except KeyError:
                count['lower'] = 0

            if count['upper'] > count['lower']:
                data_dict['upper'].append(id)
            elif count['upper'] < count['lower']:
                data_dict['lower'].append(id)
            elif count['upper'] == count['lower']:
                data_dict['fifty'].append(id)

        count_lower = len(data_dict['lower'])
        count_upper = len(data_dict['upper'])
        count_fifty = len(data_dict['fifty'])

        prc_lower = 100 * count_lower / count_all_rows
        prc_upper = 100 * count_upper / count_all_rows
        prc_fifty = 100 * count_fifty / count_all_rows

        time_analisys = (datetime.now() - start_time).total_seconds()

        return ((count_lower, prc_lower, data_dict['lower'], time_analisys),
                (count_upper, prc_upper, data_dict['upper'], time_analisys),
                (count_fifty, prc_fifty, data_dict['fifty'], time_analisys),)

    def analisys_row_with_double_spaces(self):
        start_time = datetime.now()

        data_dict = {}
        count_all_rows = 0
        print('Поиск 2х и более пробелов в наименовании')
        sleep(0.5)
        for id, namefull in tqdm(self.data_dict.items()):
            count_all_rows += 1
            search_spaces = re.findall('\s{2,}', namefull)
            if len(search_spaces) != 0:
                data_dict[id] = len(search_spaces)

        prc = 100 * len(data_dict.keys()) / count_all_rows

        time_analisys = (datetime.now() - start_time).total_seconds()

        return (len(data_dict.keys()), prc, data_dict, time_analisys)

    def get_result_files(self, top_example):
        """Метод сбора всех аналитик в один файл"""
        wb = Workbook()
        ws = wb.active
        name_column = ('Наименование анализа',
                       'Кол-во записей с ошибками',
                       '% от общего кол-ва записей',
                       'Время выполнения анализа (sec)')
        ws.append(name_column)

        analisys = self.analisys_first_classifier()
        result = ('Группы однотипных позиций',
                  analisys[0],
                  analisys[1],
                  analisys[3])
        ws.append(result)
        i = 1
        print('Формирование/запись результатов групп однотипных позиций')
        sleep(0.5)
        for group, ids in tqdm(analisys[2].items()):
            j = 1
            if i <= top_example:
                for id in ids:
                    if j <= top_example:
                        ex = ['', '', '', '']
                        val = self.get_vals_by_id(id)
                        ex.append(group)
                        ex.append(id)
                        ex.append(val)
                        ws.append(ex)
                        j += 1
                    else:
                        break
            else:
                break
            i += 1

        analisys_0 = self.analisys_double_row_potential()
        result = ('Потенциальные дубли',
                  analisys_0[0],
                  analisys_0[1],
                  analisys_0[3])
        ws.append(result)
        i = 1
        print('Формирование/запись результатов потенциальных дублей')
        sleep(0.5)
        for group, ids in tqdm(analisys_0[2].items()):
            if i <= top_example:
                for id in ids:
                    ex = ['', '', '', '']
                    val = self.get_vals_by_id(id)
                    ex.append(group)
                    ex.append(id)
                    ex.append(val)
                    ws.append(ex)
            else:
                break
            i += 1

        analisys_1 = self.analisys_double_row_fullname()
        result = ('Дубли по наименованию',
                  analisys_1[0],
                  analisys_1[1],
                  analisys_1[3])
        ws.append(result)
        i = 1
        print('Формирование/запись результатов дублей по наименованию')
        sleep(0.5)
        for group, ids in tqdm(analisys_1[2].items()):
            if i <= top_example:
                for id in ids:
                    ex = ['', '', '', '']
                    val = self.get_vals_by_id(id)
                    ex.append(group)
                    ex.append(id)
                    ex.append(val)
                    ws.append(ex)
            else:
                break
            i += 1

        analisys_2 = self.analisys_double_row_similar_name()
        result = ('Дубли по наименованию с разным раположением',
                  analisys_2[0],
                  analisys_2[1],
                  analisys_2[3])
        ws.append(result)
        i = 1
        print('Формирование/запись результатов дублей по наименованию с разным раположением')
        sleep(0.5)
        for group, ids in tqdm(analisys_2[2].items()):
            if i <= top_example:
                for id in ids:
                    ex = ['', '', '', '']
                    val = self.get_vals_by_id(id)
                    ex.append(group)
                    ex.append(id)
                    ex.append(val)
                    ws.append(ex)
            else:
                break
            i += 1

        analisys_11 = self.analisys_row_one_word()
        result = ('Позиции с одним словом в наименовании без характеристики',
                  analisys_11[0],
                  analisys_11[1],
                  analisys_11[3])
        ws.append(result)
        i = 1
        print('Формирование/запись результатов позиций с одним словом в наименовании без характеристики')
        sleep(0.5)
        for id in tqdm(analisys_11[2]):
            if i <= top_example:
                ex = ['', '', '', '', '']
                val = self.get_vals_by_id(id)
                ex.append(id)
                ex.append(val)
                ws.append(ex)
                i += 1
            else:
                break

        analisys_3 = self.analisys_row_begin_symbol()
        result = ('Позиции, которые начинаются со спецсимволов [ .,;:!@#$%"<>^&*()_+''|\\/?=]',
                  analisys_3[0],
                  analisys_3[1],
                  analisys_3[3])
        ws.append(result)
        i = 1
        print(
            'Формирование/запись результатов позиций, которые начинаются со спецсимволов [ .,;:!@#$%"<>^&*()_+''|\\/?=]')
        sleep(0.5)
        for id in tqdm(analisys_3[2]):
            if i <= top_example:
                ex = ['', '', '', '', '']
                val = self.get_vals_by_id(id)
                ex.append(id)
                ex.append(val)
                ws.append(ex)
                i += 1
            else:
                break

        analisys_4 = self.analisys_row_begin_int()
        result = ('Позиции, которые начинаются с чисел',
                  analisys_4[0],
                  analisys_4[1],
                  analisys_4[3])
        ws.append(result)
        i = 1
        print('Формирование/запись результатов позиций, которые начинаются с чисел')
        sleep(0.5)
        for id in tqdm(analisys_4[2]):
            if i <= top_example:
                ex = ['', '', '', '', '']
                val = self.get_vals_by_id(id)
                ex.append(id)
                ex.append(val)
                ws.append(ex)
                i += 1
            else:
                break

        analisys_5 = self.analisys_row_begin_latin()
        result = ('Позиции, которые начинаются с латинских букв',
                  analisys_5[0],
                  analisys_5[1],
                  analisys_5[3])
        ws.append(result)
        i = 1
        print('Формирование/запись результатов позиций, которые начинаются с латинских букв')
        sleep(0.5)
        for id in tqdm(analisys_5[2]):
            if i <= top_example:
                ex = ['', '', '', '', '']
                val = self.get_vals_by_id(id)
                ex.append(id)
                ex.append(val)
                ws.append(ex)
                i += 1
            else:
                break

        analisys_6 = self.analisys_row_begin_adj()
        result = ('Позиции, которые начинаются с прилагательных',
                  analisys_6[0],
                  analisys_6[1],
                  analisys_6[3])
        ws.append(result)
        i = 1
        print('Формирование/запись результатов позиций, которые начинаются с прилагательных')
        sleep(0.5)
        for id in tqdm(analisys_6[2]):
            if i <= top_example:
                ex = ['', '', '', '', '']
                val = self.get_vals_by_id(id)
                ex.append(id)
                ex.append(val)
                ws.append(ex)
                i += 1
            else:
                break

        analisys_7 = self.analisys_row_symbol_inside_word()
        result = ('Символы [!@#$%<>^&_+''|?] внутри слов',
                  analisys_7[0],
                  analisys_7[1],
                  analisys_7[3])
        ws.append(result)
        i = 1
        print('Формирование/запись результатов, где символы [!@#$%<>^&_+''|?] внутри слов')
        sleep(0.5)
        for id, words in tqdm(analisys_7[2].items()):
            if i <= top_example:
                for w in words.keys():
                    ex = ['', '', '', '', '']
                    val = self.get_vals_by_id(id)
                    ex.append(id)
                    ex.append(val)
                    ex.append(w)
                    ex.append(','.join(words[w]))
                    ws.append(ex)
            else:
                break
            i += 1

        analisys_8 = self.analisys_row_latin_inside_ruword()
        result = ('Латинские буквы внутри русских слов',
                  analisys_8[0],
                  analisys_8[1],
                  analisys_8[3])
        ws.append(result)
        i = 1
        print('Формирование/запись результатов, где латинские буквы внутри русских слов')
        sleep(0.5)
        for id, words in tqdm(analisys_8[2].items()):
            if i <= top_example:
                for w in words.keys():
                    ex = ['', '', '', '', '']
                    val = self.get_vals_by_id(id)
                    ex.append(id)
                    ex.append(val)
                    ex.append(w)
                    ex.append(','.join(words[w]))
                    ws.append(ex)
            else:
                break
            i += 1

        analisys_9 = self.analisys_row_cyrillic_inside_enword()
        result = ('Русские буквы внутри латинских слов',
                  analisys_9[0],
                  analisys_9[1],
                  analisys_9[3])
        ws.append(result)
        i = 1
        print('Формирование/запись результатов, где русские буквы внутри латинских слов')
        sleep(0.5)
        for id, words in tqdm(analisys_9[2].items()):
            if i <= top_example:
                for w in words.keys():
                    ex = ['', '', '', '', '']
                    val = self.get_vals_by_id(id)
                    ex.append(id)
                    ex.append(val)
                    ex.append(w)
                    ex.append(','.join(words[w]))
                    ws.append(ex)
            else:
                break
            i += 1

        analisys_10 = self.analisys_row_yo_inside_word()
        result = ('Буква Ё внутри слов',
                  analisys_10[0],
                  analisys_10[1],
                  analisys_10[3])
        ws.append(result)
        i = 1
        print('Формирование/запись результатов, где буква Ё внутри слов')
        sleep(0.5)
        for id, words in tqdm(analisys_10[2].items()):
            if i <= top_example:
                for w in words.keys():
                    ex = ['', '', '', '', '']
                    val = self.get_vals_by_id(id)
                    ex.append(id)
                    ex.append(val)
                    ex.append(w)
                    ex.append(','.join(words[w]))
                    ws.append(ex)
            else:
                break
            i += 1

        analisys_12 = self.analisys_row_with_non_breaking_space()
        result = ('Неразрывный пробел внутри слов',
                  analisys_12[0],
                  analisys_12[1],
                  analisys_12[3])
        ws.append(result)
        i = 1
        print('Формирование/запись результатов, где неразрывный пробел внутри слов')
        sleep(0.5)
        for id, words in tqdm(analisys_12[2].items()):
            if i <= top_example:
                for w in words.keys():
                    ex = ['', '', '', '', '']
                    val = self.get_vals_by_id(id)
                    ex.append(id)
                    ex.append(val)
                    ex.append(w)
                    ex.append(words[w])
                    ws.append(ex)
            else:
                break
            i += 1

        analisys_13 = self.analisys_row_with_tab_space()
        result = ('Пробел в виде tab внутри слов',
                  analisys_13[0],
                  analisys_13[1],
                  analisys_13[3])
        ws.append(result)
        i = 1
        print('Формирование/запись результатов, где пробел в виде tab внутри слов')
        sleep(0.5)
        for id, words in tqdm(analisys_13[2].items()):
            if i <= top_example:
                for w in words.keys():
                    ex = ['', '', '', '', '']
                    val = self.get_vals_by_id(id)
                    ex.append(id)
                    ex.append(val)
                    ex.append(w)
                    ex.append(words[w])
                    ws.append(ex)
            else:
                break
            i += 1

        analisys_14 = self.analisys_row_with_spelling_mistakes()
        result = ('Орфографические ошибки',
                  analisys_14[0],
                  analisys_14[1],
                  analisys_14[3])
        ws.append(result)
        i = 1
        print('Формирование/запись результатов, с орфографическими ошибками')
        sleep(0.5)
        for id, errors in tqdm(analisys_14[2].items()):
            if i <= top_example:
                ex = ['', '', '', '', '']
                val = self.get_vals_by_id(id)
                ex.append(id)
                ex.append(val)
                ex.append(errors)
                ws.append(ex)
            else:
                break
            i += 1

        analisys_15 = self.analisys_row_with_upper_lower_name()
        result_lower = ('Наименования в нижнем регистре',
                  analisys_15[0][0],
                  analisys_15[0][1],
                  analisys_15[0][3])
        ws.append(result_lower)
        i = 1
        print('Формирование/запись результатов наименований в нижнем регистре')
        sleep(0.5)
        for id in tqdm(analisys_15[0][2]):
            if i <= top_example:
                ex = ['', '', '', '', '']
                val = self.get_vals_by_id(id)
                ex.append(id)
                ex.append(val)
                ws.append(ex)
            else:
                break
            i += 1

        result_upper = ('Наименования в верхнем регистре',
                        analisys_15[1][0],
                        analisys_15[1][1],
                        analisys_15[1][3])
        ws.append(result_upper)
        i = 1
        print('Формирование/запись результатов наименований в верхнем регистре')
        sleep(0.5)
        for id in tqdm(analisys_15[1][2]):
            if i <= top_example:
                ex = ['', '', '', '', '']
                val = self.get_vals_by_id(id)
                ex.append(id)
                ex.append(val)
                ws.append(ex)
            else:
                break
            i += 1

        result_fifty = ('Наименования и в нижнем и в верхнем регистре',
                        analisys_15[2][0],
                        analisys_15[2][1],
                        analisys_15[2][3])
        ws.append(result_fifty)
        i = 1
        print('Формирование/запись результатов наименований и в нижнем и в верхнем регистре')
        sleep(0.5)
        for id in tqdm(analisys_15[2][2]):
            if i <= top_example:
                ex = ['', '', '', '', '']
                val = self.get_vals_by_id(id)
                ex.append(id)
                ex.append(val)
                ws.append(ex)
            else:
                break
            i += 1

        analisys_16 = self.analisys_row_with_double_spaces()
        result = ('2 и более пробелов внутри наименований',
                  analisys_16[0],
                  analisys_16[1],
                  analisys_16[3])
        ws.append(result)
        i = 1
        print('Формирование/запись результатов, с 2 и более пробелов внутри наименований')
        sleep(0.5)
        for id, errors in tqdm(analisys_16[2].items()):
            if i <= top_example:
                ex = ['', '', '', '', '']
                val = self.get_vals_by_id(id)
                ex.append(id)
                ex.append(val)
                ex.append(errors)
                ws.append(ex)
            else:
                break
            i += 1

        result_file = os.path.join(self.path_file, 'analysis_data.xlsx')
        wb.save(result_file)


if __name__ == '__main__':
    from datetime import datetime

    path = 'D:\\users\\dkochubej\\Documents\\Проекты\\presale_analysis\\git\\app\\etc'

    start_time = datetime.now()

    presale = PreSaleAnalisys(path_file=path, file_name='Data2.xlsx')
    presale.get_result_files(top_example=1000000)

    print('Затраченное время на анализ: {}'.format((datetime.now() - start_time).total_seconds()))
