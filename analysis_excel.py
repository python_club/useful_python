# Анализатор файлов EXCEL
# На воход получает файлы, собирает информацию в отдельную таблицу
import os
import sys
import pyexcel as p
from openpyxl import Workbook
import hashlib
from collections import Counter

# Запрашиваем ввод адрес, где располагаются файлы Excel в формате "D:\users\..."
path = input('Enter folder address:').replace('/', '\\')

# Создаем папку, где будем хранить файл с анализом
path_export = (path + '\\' + 'analysis_excel_files').replace('/', '\\')
if not os.path.exists(path_export):
    os.makedirs(path_export)

# Читаем список файлов в указанной папке
files = os.listdir(path)

# Создаем список с именами файлов с разрешением .xls и .xlsx
files_excel = [i for i in files if i.endswith('.xls') or i.endswith('.xlsx')]

# Активируем книгу EXCEL для записи
wb = Workbook()
ws = wb.active
# Названия столбцов
name_column = ('Имя файла',
               'Имя листа',
               'Размер файла',
               'Кол-во строк',
               'Кол-во столбцов',
               'Кол-во заполненных ячеек',
               'Средний размер ячейки',
               'Кол-во задублированных строк')
# Записываем названия столбцов
ws.append(name_column)
# Открываем поочередно найденные в папке файлы
for file in files_excel:
    # создаем корректный адрес файла
    path_file = (path + '\\' + file).replace('/', '\\')
    # создаем словарь данных файла
    filling_file = p.get_book_dict(file_name=path_file)
    # открываем поочередно листы из книги
    for sheet in filling_file:
        # создаем результирующий массив для добавления данных
        result = []
        # добавляем в результат имя файла
        result.append(file)
        # добавляем в результат имя листа
        result.append(sheet)
        # добавляем в результат размер файла в КБ
        result.append('{} КБ'.format(float("{0:.2f}".format(os.path.getsize(path_file)/1000))))
        # добавляем в результат кол-во строк
        result.append(len(filling_file[sheet]))
        # добавляем в результат кол-во столбцов
        result.append(max([len(i) for i in filling_file[sheet]]))
        # создаем массив из всех данных на листе
        data_all = [i for i in filling_file[sheet]]
        # создаем массив для подсчетов
        filling_cell = []
        # открываем каждую строку поочередно
        for row in data_all:
            # берем каждое значение кроме пустых
            for cell in row:
                if cell != '':
                    # добавляем не пустые значения
                    filling_cell.append(cell)
        # добавляем в результат кол-во заполненных ячеек
        result.append(len(filling_cell))
        # рассчитываем размер каждой ячейки
        size_cell = [(sys.getsizeof(i)/8) for i in filling_cell]
        # добавляем в результат средний размер ячейки
        result.append('{} Б'.format((float("{0:.2f}".format(sum(size_cell) / len(size_cell))))))
        # генерируем массив данных одного листа
        data_all = [i for i in filling_file[sheet]]
        # создаем мвссив для добавления хэшированных данных строк
        count_row = []
        # обрабатываем каждую строку по отдельности
        for row in data_all:
            # объединяем массив данных 1й строки в преобразовываем в строку по байтам
            lip_row = ''.join(map(str, row)).encode()
            # хэшируем полученную сверху строку
            hash_row = hashlib.md5(lip_row).hexdigest()
            # добавляем хэш в выше созданный массив
            count_row.append(hash_row)
        # подсчитываем одинаковые занечние в массеве с хэшем и преобразуем в словарь
        # где ключ хэш, значение - кол-во повторений
        count_double_row = Counter(count_row)
        # массив для подсчета значений
        sum_count = 0
        # читаем полученный словарь
        for number_row, sum_row in count_double_row.items():
            # обрабатываем только те строки которые повоторяются более 1го раза
            if sum_row > 1:
                sum_count = sum_count + sum_row
        # добавляем в результат кол-во дублей
        result.append(sum_count)
        # записываем результат в файл
        ws.append(result)
# Сохраняем/закрываем файл
wb.save((path_export + '\\' + 'analysis' + '.xlsx').replace('/', '\\'))