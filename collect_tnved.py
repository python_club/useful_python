import os
import wget
import zipfile
import re
from openpyxl import Workbook
from tqdm import tqdm
from pprint import pprint

url_catalog_tnved = 'https://www.nalog.ru/html/sites/www.new.nalog.ru/docs/sprav/TNVED.ZIP'
path = os.path.join(os.getcwd(), 'upload')

wget.download(url_catalog_tnved,path)

file_path = os.path.join(path,'TNVED.ZIP')

zipfile = zipfile.ZipFile(file_path)
zipfile.extractall(path)
zipfile.close()

os.remove(file_path)

path_file = os.path.join(path, 'TNVED3.TXT')

tnved_dict = {}

with open(path_file, encoding='cp866') as f:
    for line in f.readlines():
        arr_line = line.split('|')
        try:
            if arr_line[4] == '':
                code = str(arr_line[0]) + str(arr_line[1]) + '000'
                name = ' '.join([i for i in re.split('[ \xa0]', arr_line[2]) if i != ''])
                tnved_dict[code] = [name.capitalize(), {}]
        except IndexError:
            continue
os.remove(path_file)

path_file = os.path.join(path, 'TNVED4.TXT')
with open(path_file, encoding='cp866') as f:
    for line in f.readlines():
        arr_line = line.split('|')
        try:
            if arr_line[5] == '':
                code = str(arr_line[0]) + str(arr_line[1]) + str(arr_line[2])
                first_parcode = code[:4] + '000'
                second_parcode = code[:6] + '00'
                name = ' '.join([i for i in re.split('[- \xa0]', arr_line[3]) if i != ''])
                try:
                    parent_name = ' '.join(tnved_dict[first_parcode][0].split(' ')[:4])
                    full_name = parent_name + '..., ' + name
                except KeyError:
                    full_name = name
                try:
                    tnved_dict[first_parcode][1][second_parcode][1].update({code: [name, ]})
                except KeyError:
                    try:
                        tnved_dict[first_parcode][1].update({second_parcode: [parent_name, {}]})
                        tnved_dict[first_parcode][1][second_parcode][1].update({code: [name, ]})
                    except KeyError:
                        tnved_dict[first_parcode] = [parent_name, {}]
                        tnved_dict[first_parcode][1].update({code: [name, ]})
        except IndexError:
            continue

os.remove(path_file)

# Активируем книгу EXCEL для записи
wb = Workbook()
ws = wb.active
# Названия столбцов
name_column = ('code',
               'parcode',
               'name',)
# Записываем названия столбцов
ws.append(name_column)

for parcode, tree in tqdm(tnved_dict.items()):
    ws.append([parcode, '', tree[0]])
    for code, tr in tree[1].items():
        ws.append([code, parcode, tr[0]])
        try:
            for cd, t in tr[1].items():
                ws.append([cd, code, tr[0] + '..., ' + t[0]])
        except IndexError:
            continue

wb.save(('{}\\{}.xlsx'.format(path, 'TNVED')).replace('/', '\\'))
