import re
import random


class GenerateAuth:
    def __init__(self, full_name):
        self.full_name = full_name

    def get_account(self):
        ru_to_latin_latters = {
            'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e', 'ё': 'yo', 'ж': 'zh',
            'з': 'z', 'и': 'i', 'й': 'y', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n', 'о': 'o', 'п': 'p',
            'р': 'r', 'с': 's', 'т': 't', 'у': 'u', 'ф': 'f', 'х': 'kh', 'ц': 'ts', 'ч': 'ch',
            'ш': 'sh', 'щ': 'shch', 'ъ': '»', 'ы': 'y', 'ь': '‘', 'э': 'e', 'ю': 'yu', 'я': 'ya',
        }

        symbol_password = 'abcdefghijkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789!@#$%^&*()_+{}[]\|/<>,.'

        arr_full_name = self.full_name.lower().split()

        login = []

        if len(re.findall('[a-z]', arr_full_name[1][:1])) == 1:
            login.append(arr_full_name[1][:1])
        else:
            login.append(ru_to_latin_latters[arr_full_name[1][:1]])

        for lt in arr_full_name[0]:
            if len(re.findall('[a-z]', lt)) == 1:
                login.append(lt)
            else:
                login.append(ru_to_latin_latters[lt])

        password = []

        while len(password) < 12:
            random_int = random.randint(0, len(symbol_password) - 1)
            if random_int in password:
                continue
            else:
                password.append(random_int)

        return ''.join(login), ''.join([symbol_password[s] for s in password])


if __name__ == '__main__':

    from collections import Counter

    auth = GenerateAuth('Кочубей Денис').get_account()
    print(auth)

    pswds = []
    i=0
    while i <= 1000000:
        auth = GenerateAuth('Кочубей Денис').get_account()
        pswds.append(auth[1])
        i += 1

    print({k:v for k,v in Counter(pswds).items() if v != 1})