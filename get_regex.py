import numpy as np


class StrToRegex:
    """Модуль создания шаблона регулярного выражения на основе полученного примера"""

    def get_regex(str):
        """Метод получения регулярного выражения из шаблона
        str - на вход передается строка для примера"""
        reg = []
        # преобразовываем каждый символ из переданной строки
        for v in str:
            # если символ число
            if v.isnumeric():
                reg.append('d')
            # если символ буква
            elif v.isalpha():
                reg.append('[a-яA-Я]')
                # reg.append('w')
            # если какой-либо символ
            else:
                reg.append('W')
                # reg.append('.')
        result = ''
        i = 0
        # подсчитываем и формируем регулярное выражение из собранного выше массива
        for v in reg:
            try:
                v == reg[1] and i + 1 != len(reg)
            except IndexError:
                i = 1
                if v != '[a-яA-Я]':
                    result += '(\\{}{{{}}})'.format(v, i)
                    reg = np.delete(reg, [j for j in range(0, i)]).tolist()
                else:
                    result += '({}{{{}}})'.format(v, i)
                    reg = np.delete(reg, [j for j in range(0, i)]).tolist()
            else:
                if v == reg[1] and i + 1 != len(reg):
                    i += 1
                else:
                    if reg[i - 1] != '[a-яA-Я]':
                        if i >= 1:
                            result += '(\\{}{{{}}})'.format(reg[i - 1], i)
                        reg = np.delete(reg, [j for j in range(0, i)]).tolist()
                        i = 1
                    else:
                        if i >= 1:
                            result += '({}{{{}}})'.format(reg[i - 1], i)
                        reg = np.delete(reg, [j for j in range(0, i)]).tolist()
                        i = 1
                    if len(reg) != 0:
                        try:
                            if v != reg[1]:
                                if v != '[a-яA-Я]':
                                    if i >= 1:
                                        result += '(\\{}{{{}}})'.format(v, i)
                                    reg = np.delete(reg, [j for j in range(0, i)]).tolist()
                                    i = 0
                                else:
                                    if i >= 1:
                                        result += '({}{{{}}})'.format(v, i)
                                    reg = np.delete(reg, [j for j in range(0, i)]).tolist()
                                    i = 0
                        except IndexError:
                            if v != '[a-яA-Я]':
                                if i >= 1:
                                    result += '(\\{}{{{}}})'.format(v, i)
                                reg = np.delete(reg, [j for j in range(0, i)]).tolist()
                            else:
                                if i >= 1:
                                    result += '({}{{{}}})'.format(v, i)
                                reg = np.delete(reg, [j for j in range(0, i)]).tolist()

        return '({})'.format(result)

if __name__ == '__main__':
    import re

    atr1 = 'мешки'
    atr2 = '2/1(п)'
    atr3 = 'зак 52х49'
    atr4 = '53х38х11'
    atr5 = 'УМК 19-1898'
    atr6 = '1502S'
    atr7 = 'Торговый Дом Сегежский'

    regex = {'atr1': '^'+StrToRegex.get_regex(atr1),  # генерация регулярного выражения
             'atr2': StrToRegex.get_regex(atr2),
             'atr3': StrToRegex.get_regex(atr3),
             'atr4': StrToRegex.get_regex(atr4),
             'atr5': StrToRegex.get_regex(atr5),
             'atr6': StrToRegex.get_regex(atr6),
             'atr7': StrToRegex.get_regex(atr7)}

    text = 'мешки 2/1(п) зак 52х49,5х11 УМК 17-3974, 1502К, Торговый Дом Сегежский'  # строка для поиска
    text2 = 'мешки 2/1(п) зак 53х38х11 УМК 18-6300, 3862А, ЦентралПак'

    result = {}
    for a,r in regex.items():
        print(r)
        result[a] = [i[0] for i in re.findall(r, text2)]

    print(result)