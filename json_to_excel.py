# Конвертер JSON в EXCEL
import os
import json
from openpyxl import Workbook

# Запрашиваем ввод адрес, где располагаются файлы JSON в формате "D:\users\..."
path = input('Enter folder address:').replace('/', '\\')

# Создаем папку, где будем хранить конвертированные файлы
path_export = (path + '\\' + 'convert_json_to_excel').replace('/', '\\')
if not os.path.exists(path_export):
    os.makedirs(path_export)

# Читаем список файлов в указанной папке
files = os.listdir(path)

# Создаем список с именами файлов с разрешением JSON
files_json = [i for i in files if i.endswith('.json')]

# Обрабатываем по одному файлу
for name_file in files_json:

    # создаем путь до необходимого файла
    file = (path + '\\' + name_file).replace('/', '\\')
    # активируем инструмент и книгу по работе с ECXEL
    wb = Workbook()
    ws = wb.active
    # Открываем файл JSON в юникоде на чтение
    with open(file, 'r', encoding='utf8') as f:
        # создаем словарь из файла
        data = json.loads(f.read())
        # записываем название полей из 0ого значения списка ROOT
        name_column = []
        # в этом моменте могут быть ошибки так как название поля "Данные" может отличаться
        for key in data['root'][0]['Данные']:
            name_column.append(key)
        ws.append(name_column)
        # записываем поочередно все значения в файл
        for i in range(1, len(data['root']) - 1):
            new_row = []
            for column in name_column:
                # здесь должен быть код по расхэшированию значения
                # в этом моменте могут быть ошибки так как название поля "Данные" и "Значение" могут отличаться
                new_row.append(data['root'][i]['Данные'][column]['Значение'])
            ws.append(new_row)
    # сохраняем файл
    wb.save((path_export + '\\' + name_file + '.xlsx').replace('/', '\\'))
