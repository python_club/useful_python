# Анализатор сайта с выгрузкой в EXCEL данных о времени загрузки и прочего

# Также необходимо установить pip install speedtest-cli
from selenium import webdriver
import time
from openpyxl import Workbook
import subprocess
import json
from datetime import datetime

# скачать https://sites.google.com/a/chromium.org/chromedriver/downloads
# и положить в папку со своим юзером C:\Users\user\AppData\Local\Programs\Python\Python37-32\Scripts
# задаем параметры для вебдрайвера
driver = webdriver.ChromeOptions()

# задаем параметр работы в фоне, чтобы наблюдать что происходит в браузере, можно это строку закомментить
driver.add_argument('--headless')

# передаем параметры вебдрайверу
browser = webdriver.Chrome(options=driver)

# 10 сек в качестве максимального таймаута для выполнения методов
browser.implicitly_wait(30)

# для начала открываем страницу авторизации, если таковая есть
browser.get('http://www.site.test.com/')  

# Находим название класса поля для ввода логина, в нашем случае "ant-input" (изменить на нужный!)
login = browser.find_element_by_class_name("ant-input")

# login.clear() - функция очистки поля
# Вводим имя пользователя
login.send_keys("admin")

# Находим тип поля для ввода пароля, в нашем случае "password" (изменить на нужный!)
pswd = browser.find_element_by_xpath("//input[@type='password']")

# Вводим пароль
pswd.send_keys("admin")

# Находим тип поля кнопки входа, в нашем случае "submit" (изменить на нужный!) и нажимаем на нее click()
browser.find_element_by_xpath("//button[@type='submit']").click()

# создаем функцию, которая будет постоянно нажимать на кнопку следующей страницы
def next_page(browser):
    # находим название класса кнопки NEXT (изменить на нужный!) и нажимаем на нее
    browser.find_element_by_class_name('ant-pagination-next').click()

# создаем функцию, которая будет собирать все ссылки со страницы
def search_links(browser):
    # создаем пустой словарь для хранения собранных ссылок и их названий
    all_links = {}

    # ищем сслыки с тегом <a> и аотрибутом href на переданной странице (изменить на нужные!)
    elements = browser.find_elements_by_xpath("//a[@href]")

    # перечитываем элемент elements
    for link in elements:
        # добавляем в выше созданный словарь полученные значения
        all_links.update({link.text:link.get_attribute('href')})

    # функция после выполнения вернет словарь
    return all_links

# создаем словарь, куда будем сохранять ВСЕ страницы
all_all_links = {}

# здесь поговорим, что пока кнопка NEXT активна продолжаем на нее нажимать
while browser.find_element_by_class_name('ant-pagination-next').get_attribute('aria-disabled') == 'false':

    # нажали на кнопку NEXT
    next_page(browser)

    # отсканировали страницу на ссылки
    for k, v in search_links(browser).items():

        # добавили все ссылки в общий словарь
        all_all_links.update({k:v})
    continue

# Активируем excel
wb = Workbook()
ws = wb.active

# Создаем названия столбцов
name_column = ('Имя доски',
               'Адрес доски',
               'Кол-во обновлений',
               'Кол-во виджетов на доске',
               'Среднее время обновления всех элементов на странице, мс.',
               'Среднее время обновления одного виджета, мс.',
               'Скорость соединения Download, Mbit/s',
               'Средний размер доски, Mbit',
               'Средний размер 1го виждета, Mbit',
               'Скорость соединения Upload, Mbit/s',
               'Время анализа 1-й страницы, sec')

# Записываем названия столбцов в файл
ws.append(name_column)

# получем кол-во собранных ссылок
top = len(all_all_links.keys())

# если ссылок много, можно изучить топ-n'ое кол-во, раскомментировав строку ниже
# top = 10
# начинаем открывать каждую страницу из собранного словаря выше
for name,link in all_all_links.items():

    # пока значение
    if top != 0:
        # создаем массив для записи собранных данных в Excel
        result = []

        # получаем дату начала обработки страницы (для логов и прочих рассчетов)
        time_start = datetime.now()

        # добавляем в результирующий массив имя ссылки
        result.append(name)

        # добавляем в результирующий массив ссылку
        result.append(link)

        # запускаем в консоли скрипт speedtest --json,
        # от выше установленного пакета pip install speedtest-cli
        p = subprocess.Popen('speedtest --json', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

        # загружаем полученный объект в виде JSON
        speed_data = json.loads(p.stdout.readlines()[0])

        # вытаскиваем скорость загрузки и преобразовываем ее в FLOAT с 2я знаками после запятой
        speed_download = float(('{}'.format((float("{0:.2f}".format(speed_data['download']/1000000))))))

        # вытаскиваем скорость отдачи и преобразовываем ее в FLOAT с 2я знаками после запятой
        speed_upload = float(('{}'.format((float("{0:.2f}".format(speed_data['upload']/1000000))))))

        # открываем ссылку через вебдрайвер
        browser.get(link)

        # задаем число обновлений страницы
        loops = 5

        # добавляем кол-во обновлений в результирующий массив
        result.append(loops)

        # считаем кол-во интересующих блоков контента, указывая их класс,
        # в нашем случае "react-grid-item" (изменить на нужные!)
        count_widgets = len(browser.find_elements_by_class_name('react-grid-item'))

        # добавляем кол-во блоков с контентом в результирующий массив
        result.append(count_widgets)

        # создаем переменную для подсчета времени
        total = 0

        # в случае, если сайт работает с куками, их нужно проставить,
        # напр., так:
        browser.execute_script( "document.cookie='name=value'" )

        # обновляем
        for j in range(loops):
            # обновляем вебдрайвер с выше указанной ссылкой
            browser.refresh()

            # ждем 3 сек загрузку страницы, раскомментировать для Chrome!
            time.sleep(3) 

            # получаем число миллисекунд для браузерного события onload
            stext = browser.execute_script(
                "return ( window.performance.timing.loadEventEnd - window.performance.timing.navigationStart )")

            # суммируем с выше заведенным значением total
            total = total + int(stext)

        # считаем среднее время загрузки 1-й страницы
        avg_time = float(('{}'.format((float("{0:.2f}".format(total / loops))))))

        # добавляем среднее время загрузки в результирующий массив
        result.append(avg_time)

        # если кол-во нужных блоков с контентом будет нулевым,
        # то мы не сможем посчитать время на 1 блок, поэтому 
        # считаем время на загрузку одного блока, если кол-во НЕ НУЛЕВОЕ
        if count_widgets != 0:
            widg = float('{}'.format((float("{0:.2f}".format(avg_time / count_widgets)))))
            # добавляем в результирующий массив
            result.append(widg)
        else:
            # если кол-во блоков все таки 0, то добавляем 'N/D'
            widg = 'N/D'
            # добавляем 'N/D' в результирующий массив
            result.append(widg)

        # добавляем в результирующий массив скорость загрузки
        result.append(speed_download)

        # добавляем в результирующий массив скорость загрузки всей страницы
        result.append(float(('{}'.format((float("{0:.2f}".format((avg_time * speed_download)/1000)))))))

        # если у нас нет виджетов, время загрузки тоже заменяем на 'N/D'
        if widg != 'N/D':

            # добавляем в результирующий массив скорость загрузки 1 блока
            result.append(float(('{}'.format((float("{0:.2f}".format((widg * speed_download) / 1000)))))))

        else:
            # добавляем 'N/D' в результирующий массив, если кол-во блоков нулевое
            result.append('N/D')

        #  добавляем в результирующий массив скорость отдачи
        result.append(speed_upload)

        # узнаем время на текущий момент
        time_end = datetime.now()

        # считаем время выполнения обработки 1-й страницы
        delta = time_end - time_start

        #  добавляем в результирующий массив время обработки страницы
        result.append(delta.total_seconds())

        # открываем лог
        log_file = open('log.txt', 'a')

        # дозаписываем инф-ии
        # дата и время запуска
        # кол-во секунд выполнения
        # остаток страниц
        # ссылка
        # имя
        log_file.write('{} {} {} {} {}\n'.format(datetime.now(), delta.total_seconds(),top-1, link, name))

        # закрываем лог файл
        log_file.close()

        # добавляем данные в Excel
        ws.append(result)

        # вычитаем еденицу от цыкла, говоря что 1а страница обработана
        top -= 1

        # сохраняем все в файл EXCEL
        wb.save('analysis.xlsx')

        # возвращаемся в цикл
        continue
    else:
        break

# выходим из вебдрайвера
browser.quit()
