# Поиск дублей строк в EXCEL
# На выходе папка и файл с анализом
import os
import pyexcel as p
from openpyxl import Workbook
import hashlib
from collections import Counter

# Запрашиваем ввод адрес, где располагаются файлы Excel в формате "D:\users\..."
path = input('Enter folder address:').replace('/', '\\')

# Создаем папку, где будем хранить файл с анализом
path_export = (path + '\\' + 'analysis_excel_files').replace('/', '\\')
if not os.path.exists(path_export):
    os.makedirs(path_export)

# Читаем список файлов в указанной папке
files = os.listdir(path)

# Создаем список с именами файлов с разрешением .xls и .xlsx
files_excel = [i for i in files if i.endswith('.xls') or i.endswith('.xlsx')]

# Активируем книгу EXCEL для записи
wb = Workbook()
ws = wb.active

# Открываем каждый файл из папки по отдельности
for file in files_excel:
    # создаем адрес файла
    path_file = (path + '\\' + file).replace('/', '\\')
    # преобразовываем файл в словарь
    filling_file = p.get_book_dict(file_name=path_file)
    # открываем каждый лист книги по отдельности
    for sheet in filling_file:
        # генерируем массив данных одного листа
        data_all = [i for i in filling_file[sheet]]
        # создаем мвссив для добавления хэшированных данных строк
        count_row = []
        # обрабатываем каждую строку по отдельности
        for row in data_all:
            # объединяем массив данных 1й строки в преобразовываем в строку по байтам
            lip_row = ''.join(map(str,row)).encode()
            # хэшируем полученную сверху строку
            hash_row = hashlib.md5(lip_row).hexdigest()
            # добавляем хэш в выше созданный массив
            count_row.append(hash_row)
        # подсчитываем одинаковые занечние в массеве с хэшем и преобразуем в ловарь
        # где ключ хэш, значение - кол-во повторений
        count_double_row = Counter(count_row)
        # читаем полученный словарь
        for number_row, sum in count_double_row.items():
            # результирующий массив для записи в файл
            result = []
            # обрабатываем только те строки которые повоторяются более 1го раза
            if sum > 1:
                # записываем имя файла
                result.append(file)
                # записываем имя листа
                result.append(sheet)
                # записываем хэш дублирующейся строки
                result.append(number_row)
                # записываем кол-во поторений
                result.append(sum)
                # записываем результирующий массив в файл
                ws.append(result)
# сохраняем/закрываем файл
wb.save((path_export + '\\' + 'analysis' + '.xlsx').replace('/', '\\'))